package com.example.livecodekepoo.Adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.livecodekepoo.Model.Ayahs;
import com.example.livecodekepoo.R;

import java.util.ArrayList;

public class DataAdapter extends RecyclerView.Adapter<DataAdapter.ViewHolder> {

    private ArrayList<Ayahs> android;

    public DataAdapter(ArrayList<Ayahs> android) {
        this.android = android;
    }

    @Override
    public DataAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_row,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(DataAdapter.ViewHolder holder, int position) {
        holder.tv_number.setText(android.get(position).getNumber().toString());
        holder.tv_surah.setText(android.get(position).getText());
        holder.tv_juz.setText(android.get(position).getJuz().toString());
    }

    @Override
    public int getItemCount() {
        return android.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_number;
        private TextView tv_surah;
        private TextView tv_juz;

        public ViewHolder(View itemView) {
            super(itemView);

            tv_number = (TextView)itemView.findViewById(R.id.tv_number);
            tv_surah = (TextView)itemView.findViewById(R.id.tv_surah);
            tv_juz = (TextView)itemView.findViewById(R.id.tv_juz);

        }
    }
}
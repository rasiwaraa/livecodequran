package com.example.livecodekepoo;

import com.example.livecodekepoo.Model.Surah;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;

public interface AlquranAPI {

    String BASE_URL="http://api.alquran.cloud/";

    @GET("v1/surah/1?offset=1")
    Call<Surah> getSurah();
}

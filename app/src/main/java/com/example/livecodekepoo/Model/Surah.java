package com.example.livecodekepoo.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Surah {

    @SerializedName("edition")
    @Expose
    private Edition edition;

    @SerializedName("status")
    @Expose
    private String status;

    @SerializedName("data")
    @Expose
    private Data data;


    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }


    @Override
    public String toString() {
        return "Surah{" +
                "status='" + status + '\'' +
                ", data=" + data +
                '}';
    }
}

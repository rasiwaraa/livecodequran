package com.example.livecodekepoo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.example.livecodekepoo.Adapter.DataAdapter;
import com.example.livecodekepoo.Model.Ayahs;
import com.example.livecodekepoo.Model.Surah;

import java.util.ArrayList;
import java.util.Arrays;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";

    private static final String BASE_URL = "http://api.alquran.cloud/";

    private RecyclerView recyclerView;
    private DataAdapter adapter;

    TextView tv_number_surah;
    TextView tv_name_surah;
    TextView tv_ayat;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tv_number_surah = (TextView)findViewById(R.id.tv_number_surah);
        tv_name_surah = (TextView)findViewById(R.id.tv_name_surah);
        tv_ayat = (TextView)findViewById(R.id.tv_ayat);

        recyclerView = (RecyclerView) findViewById(R.id.card_recycler_view);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        AlquranAPI alquranAPI = retrofit.create(AlquranAPI.class);
        Call<Surah> call = alquranAPI.getSurah();

        call.enqueue(new Callback<Surah>() {
            @Override
            public void onResponse(Call<Surah> call, Response<Surah> response) {
                Log.d(TAG, "onResponse: " + response.toString());
                Log.d(TAG, "onResponse: data : " + response.body().toString());

                tv_number_surah.setText(response.body().getData().getNumber().toString());
                tv_name_surah.setText("Name of Ayahs : "+response.body().getData().getEnglishName());
                tv_name_surah.setText(response.body().getData().getName());
                tv_ayat.setText("Number of Ayahs : "+response.body().getData().getNumberOfAyahs().toString());


                ArrayList<Ayahs> ayahsArrayList = response.body().getData().getAyahs();
                adapter = new DataAdapter(ayahsArrayList);
                recyclerView.setAdapter(adapter);

            }

            @Override
            public void onFailure(Call<Surah> call, Throwable t) {
                Log.e(TAG, "onFailure: " + t.getMessage());
                Toast.makeText(MainActivity.this, "Bip bip bip", Toast.LENGTH_SHORT).show();

            }
        });
    }
}
